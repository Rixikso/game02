﻿using UnityEngine;
using System.Collections;

public class HommingMissile : MonoBehaviour {
	public bool homingEnabled;
	public GameObject target;
	public float homingSensitivity =0.01f;
	public float speed_limit = 0.02f;
	public float speed = 5.0f;
	public GameObject boom;
	private Vector2 relativePos;
	private Vector3 relativePos3D;
	private Quaternion relrotation;
	private GameObject player;


	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	

	void Update () {

		if (homingEnabled) {
			relativePos = target.transform.position - transform.position;
			relativePos3D = new Vector3(relativePos.x,relativePos.y,0);
			relrotation = Quaternion.LookRotation(relativePos3D);
			transform.rotation = Quaternion.Slerp(transform.rotation,relrotation,homingSensitivity);

		}

		GetComponent<Rigidbody2D> ().AddForce (transform.forward * speed, ForceMode2D.Impulse);

		if (GetComponent<Rigidbody2D> ().velocity.magnitude > speed_limit) {
			GetComponent<Rigidbody2D> ().velocity = GetComponent<Rigidbody2D> ().velocity.normalized*speed_limit;
		}
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject.tag == "Player")
			player.GetComponent<Health>().dead_Character = true;
		Destroy (this.gameObject);
	
	}
}
