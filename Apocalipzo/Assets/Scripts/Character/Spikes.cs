﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour {

	private GameObject player;
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}



	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject == player ) 
			player.GetComponent<Health> ().dead_Character = true;
	}
}
