﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float Speed = 0f;
	public float jumpH = 0f;
	public float timeBetweenJumps = 0.5f;
	public float radiusGround = 0.2f;
	public LayerMask ground_Layers;
	public float touchReactionSite;
	public bool KeyboardMovement = false;

	private Transform circlePoint;

	private float moveX = 0f;
	private bool m_Grounded = false;
	private float YYY = 0f;
	private int jumpCounter = 0;
	private float time=0f;
	private Vector3 pos;
	private bool jump;


	void Awake(){
		circlePoint = transform.Find ("point");
	
	}
	void FixedUpdate(){


		time += Time.deltaTime;
		jump = false;
		///Touch controls działa strasznie na razie :)
		/// 
		/// use Touch control
		if (KeyboardMovement == false) {
			for (int i=0; i < Input.touchCount; i++) {
				Touch touch = Input.GetTouch (i);

				if (touch.phase == TouchPhase.Ended && Input.touchCount == 1 && touch.tapCount == 2) {
					jump = true;
				}
				if (touch.phase == TouchPhase.Began) {

					pos = Camera.main.ScreenToWorldPoint (touch.position);
					if (Input.touchCount == 1) {
						moveX = (pos.x - gameObject.transform.position.x) / 8.0f;
					}
					if (Input.touchCount == 2) {
						jump = true;
					}
				} else if (touch.phase == TouchPhase.Moved) {
					pos = Camera.main.ScreenToWorldPoint (touch.position);
					if (Input.touchCount == 1) {
						moveX = (pos.x - gameObject.transform.position.x) / 8.0f;
					}
					if (Input.touchCount == 2) {
						jump = true;
					}
				} else if (touch.phase == TouchPhase.Ended) {
					pos = Camera.main.ScreenToWorldPoint (touch.position);
					if (Input.touchCount == 1) {
						moveX = 0.0f;
					}
				}
			
				if (moveX > touchReactionSite)
					moveX = 1.0f;
				else if (moveX < -touchReactionSite)
					moveX = -1.0f;

			}

		} else {
			/// else use keyboard control
			moveX = Input.GetAxis ("Horizontal");
			jump = Input.GetButton ("Jump");

		}



		if (jump) {
			if (m_Grounded && time >= timeBetweenJumps) {
				time = 0;
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveX * Speed, jumpH);
				jumpCounter = 1;
				Debug.Log ("jump");

			} else if (jumpCounter == 1 && GetComponent<Trap> ().jumpsTimes () == 2 && time >= timeBetweenJumps) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveX * Speed, jumpH);
				jumpCounter = 2;
				time = 0;
				Debug.Log (m_Grounded);
			}
		}
		YYY = GetComponent<Rigidbody2D> ().velocity.y;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveX * Speed, YYY);

		
		if (Input.GetKey (KeyCode.Backspace)) {
			Debug.Log ("");
		}

		m_Grounded = Physics2D.OverlapCircle (circlePoint.position, radiusGround, ground_Layers);
	
	
	}
}
