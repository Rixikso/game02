﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Trap : MonoBehaviour {

	public Text redSquaresText;
	public Text blueSquaresText;

	public float yellowTrap;
	public float YellowChangeDeley;
	public float YellowChangeStep;

	public int Jumps;
	public float redJumpParam = - 0.5f;
	private int redTrap;
	private int blueTrap;
	private GameObject player;
	private float jumpHight;



	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		jumpHight = player.GetComponent<Movement> ().jumpH;
	
	}
	void OnCollisionEnter2D(Collision2D c)
	{

		if (c.gameObject.tag == "redSquare" ) {
			TriggerAutoDestroy(c.gameObject);

			redTrap++;
			modifyJumpSpeed();
		}

		if (c.gameObject.tag == "blueSquare" ) {
			TriggerAutoDestroy(c.gameObject);
			blueTrap++;
		}

		if (c.gameObject.tag == "yellowSquare" ) {
			TriggerAutoDestroy(c.gameObject);
			StartCoroutine(ChangeSlowlyYelow(8));

		}
	}
	//Update trap inventory
	void Update()
	{
		blueSquaresText.text = "" + blueTrap;
		redSquaresText.text = "" + redTrap;
	}

	public int jumpsTimes() {
		if (blueTrap >= 1)
			return 2;
		else
			return 1;
	}

	public void modifyJumpSpeed()
	{
		player.GetComponent<Movement> ().jumpH = jumpHight  + redJumpParam * redTrap;
	}

	private void TriggerAutoDestroy(GameObject obj)
	{
		obj.GetComponent<Square> ().AutoDestroy = true;
	}

	public bool MinusRedSquare(){
		if (redTrap > 0) {
			redTrap--;
			return true;
		} else
			return false;
	}

	public bool MinusYellowSquare(){
		if (yellowTrap > 0) {
			yellowTrap--;
			return true;
		} else
			return false;
	}

	public bool MinusBlueSquare(){
		if (blueTrap > 0) {
			blueTrap--;
			return true;
		} else
			return false;
	}

	IEnumerator ChangeSlowlyYelow(float newYellowTrap){
		float changeYellowSteps = 0;
		float changeYellowSign = (newYellowTrap - yellowTrap)/Mathf.Abs(((newYellowTrap - yellowTrap)));
		changeYellowSteps = Mathf.Abs(((newYellowTrap - yellowTrap) / YellowChangeStep));


		int i = 0;
		while (i<=changeYellowSteps) {
			yellowTrap+=YellowChangeStep*changeYellowSign;
			i++;

			yield return new WaitForSeconds (YellowChangeDeley);
		}
		yellowTrap = newYellowTrap;
	}

}
