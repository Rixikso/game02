﻿using UnityEngine;
using System.Collections;

public class TouchScript : MonoBehaviour {

	public GameObject some_prefab;


	private Vector3 pos;
	private float moveX;
	private float moveY;
	private float moveYstart;
	private float moveYend;
	private float time_Counter;

	void Update(){


		moveY = 0;
		for (int i=0; i < Input.touchCount; i++) {
			Touch touch = Input.GetTouch(i);

			if(touch.phase == TouchPhase.Began){
				pos = Camera.main.ScreenToWorldPoint(touch.position);
				moveYstart = pos.y;
				//????	moveX = pos.magnitude/pos.normalized;
				moveX = (pos.x - gameObject.transform.position.x)/8.0f;
			}
			else if(touch.phase == TouchPhase.Moved){
				pos = Camera.main.ScreenToWorldPoint(touch.position);

				moveX = (pos.x - gameObject.transform.position.x)/8.0f;
			}
			else if(touch.phase == TouchPhase.Ended){
				pos = Camera.main.ScreenToWorldPoint(touch.position);
				moveYend = pos.y;
				moveX=0.0f;
				moveY = moveYend - moveYstart;

			}

		}

	}
}
