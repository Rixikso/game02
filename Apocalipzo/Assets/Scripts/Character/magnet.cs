﻿using UnityEngine;
using System.Collections;

public class magnet : MonoBehaviour {

	public float coeffRED=-1;
	public float coeffBLUE = -1;
	public float coeffYELLOW = -1;
	public float squareMaxSpeed = 0.01f;

	private float R;
	private Vector2 R_vec;

	// Use this for initialization
	void Start () {

	}
	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "redSquare") { //square nearby
			R_vec = -other.gameObject.transform.position + transform.position;
			other.attachedRigidbody.AddForce (R_vec * coeffRED / (R_vec.magnitude * R_vec.magnitude*R_vec.magnitude));
			if (other.attachedRigidbody.velocity.magnitude >= squareMaxSpeed) {
				other.attachedRigidbody.velocity = new Vector2(other.attachedRigidbody.velocity.x / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed,
				 other.attachedRigidbody.velocity.y / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed);
			}

		
		}

		if (other.tag == "blueSquare") { //square nearby
			R_vec = -other.gameObject.transform.position + transform.position;
			other.attachedRigidbody.AddForce (R_vec * coeffBLUE / (R_vec.magnitude * R_vec.magnitude * R_vec.magnitude));
			if (other.attachedRigidbody.velocity.magnitude >= squareMaxSpeed) {
				other.attachedRigidbody.velocity = new Vector2 (other.attachedRigidbody.velocity.x / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed,
				                                               other.attachedRigidbody.velocity.y / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed);
			}

		}

		if (other.tag == "yellowSquare") { //square nearby
			R_vec = -other.gameObject.transform.position + transform.position;
			other.attachedRigidbody.AddForce (R_vec * coeffYELLOW / (R_vec.magnitude * R_vec.magnitude * R_vec.magnitude));
			if (other.attachedRigidbody.velocity.magnitude >= squareMaxSpeed) {
				other.attachedRigidbody.velocity = new Vector2 (other.attachedRigidbody.velocity.x / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed,
				                                                other.attachedRigidbody.velocity.y / other.attachedRigidbody.velocity.magnitude * squareMaxSpeed);
			}
			
		}


	}

	void Update () {
	
	}
}
