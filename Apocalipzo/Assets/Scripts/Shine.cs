﻿using UnityEngine;
using System.Collections;

public class Shine : MonoBehaviour {
	
	public Light point_light;
	public float initial=0;
	public float fadeSpeed = 0;



	void Start(){
		point_light.intensity = initial;

	}
	// Update is called once per frame
	void Update (){ 
		point_light.intensity = GetComponent<Trap> ().yellowTrap;
			if(GetComponent<Trap> ().yellowTrap <= 8 && GetComponent<Trap> ().yellowTrap >= 0)
				GetComponent<Trap> ().yellowTrap -= fadeSpeed*Time.deltaTime;
	}


}
