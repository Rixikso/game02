﻿using UnityEngine;
using System.Collections;

public class DoorBehaviour : MonoBehaviour {

	public GameObject nextLevel;
	private GameObject player;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	void OnTriggerEnter2D(Collider2D c)
	{
		if (player.GetComponent<Inventory> ().key && c.tag == "Player") {

			nextLevel.SetActive(true);
		}
	}
}
