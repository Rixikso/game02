﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {

	private GameObject player;
	public GameObject retry_Button;
	private GameObject cam;
	private bool menu = false;

	void DeathOfPlayer()
	{
		retry_Button.SetActive (true);
		cam.GetComponent<FollowCam> ().enabled = false;
		player.GetComponent<Movement>().enabled = false;
		menu = true;
		player.GetComponent<Health>().dead_Character = true;

	}


	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
		cam = GameObject.FindGameObjectWithTag ("MainCamera");
		retry_Button.SetActive (false);

	}
	void Update(){
		if (!menu && player.transform.position.y < -5.0f || player.GetComponent<Health>().dead_Character ) {
			DeathOfPlayer();
		}
	}

	public void ChangeSceneTo(string Scene)
	{
		Application.LoadLevel (Scene);
	}

	public void QuitScene()
	{
		Application.Quit ();
	}





}
