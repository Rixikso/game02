﻿using UnityEngine;
using System.Collections;

public class KeyBehaviour : MonoBehaviour {
	private GameObject player;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	void OnTriggerStay2D(Collider2D c)
	{
		if (c.tag == "Player") {
			player.GetComponent<Inventory>().key = true;
			Destroy(this.gameObject);
		}
	}

}
